###////////////////////////////////////////////////////////////////////////
### University of Hawaii, College of Engineering
### EE 205  - Object Oriented Programming
### Lab 04c - Hello C++
###
### This is an Object Oriented C++ Hello World program
###
### @file MAKEFILE
### @version 1.0
###
### @author Kendal Oya <kendalo@hawaii.edu>
### @brief  Lab 04c 
### @date   11 Feb 2021
###############################################################################

TARGETS = hello1 hello2

all: $(TARGETS)

hello1: hello1.cpp
	g++ -g -Wall -o hello1 	hello1.cpp

hello2: hello2.cpp
	g++ -g -Wall -o hello2	hello2.cpp

clean:
	rm -f $(TARGETS) *.o
