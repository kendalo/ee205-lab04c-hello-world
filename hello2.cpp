///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04c - Hello C++
///
/// This is an Object Oriented C++ Hello World program
///
/// @file hello2.cpp
/// @version 1.0
///
/// @author Kendal Oya <kendalo@hawaii.edu>
/// @brief  Lab 04c 
/// @date   11 Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

int main(){

   std:: cout<<"Hello World!"<<std::endl;
}
